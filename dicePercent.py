import random, pandas, sys
import matplotlib.pyplot as plt

class dice:
    def __init__(self, diceString):
        self.numberOfDice, self.diceSize = map(int, diceString.split('d'))
        self.maxRoll = self.numberOfDice*self.diceSize
    
    def roll(self):
        output = 0
        for _ in range(self.numberOfDice):
           output+=random.randint(1, self.diceSize)
        return output

def main():
    dicePool = []
    lowestRoll = 0
    highestRoll = 0
    for diceString in sys.argv[1:]:
        diceObject = dice(diceString)
        lowestRoll+=diceObject.numberOfDice
        highestRoll+=diceObject.maxRoll
        dicePool.append(diceObject)
    rolls = []
    for _ in range(200000):
        rolls.append(sum([ x.roll() for x in dicePool ]))
    rollPercent = []
    for x in range(lowestRoll, highestRoll+1):
        rollPercent.append(round((rolls.count(x)/200000)*100, 2))
    s = pandas.Series(rollPercent, index=list(range(lowestRoll, highestRoll+1)), name='series')
    pie = s.plot.pie(figsize=(6,6), autopct='%.2f')
    plt.show()
    bar = s.plot.bar()
    plt.show()

main()
