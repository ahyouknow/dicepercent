import pandas, sys
import matplotlib.pyplot as plt

class dicePool:
    def __init__(self):
        self.sums = []
        self.dices = []
        for dice in sys.argv[1:]:
            numberOfDice, diceType = map(int, dice.split('d'))
            for _ in range(numberOfDice):
                self.dices.append(diceType)

    def permutate(self, totalSum=0, position=0):
        dice = self.dices[position]
        if len(self.dices) != position+1:
            for roll in  range(1,dice+1):
                self.permutate(totalSum=totalSum+roll, position=position+1)
        else:
            for roll in range(1,dice+1):
                self.sums.append(totalSum+roll)

def main():
    dicepool = dicePool()
    dicepool.permutate()
    rollPercent = []
    for x in sorted(set(dicepool.sums)):
        rollPercent.append(round(dicepool.sums.count(x)/len(dicepool.sums)*100, 2))
    s = pandas.Series(rollPercent, index=sorted(set(dicepool.sums)), name='series')
    pie = s.plot.pie(figsize=(6,6), autopct='%.2f')
    plt.show()
    bar = s.plot.bar()
    plt.show()

main()
